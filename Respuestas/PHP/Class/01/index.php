<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Numbers {

    public function Numbers() {
        
    }

    public function sum($a, $b) {        
        $total = $a + $b;
        echo $a.' + '.$b.' = '.$total;
        echo '<br>';
        //return $total;
    }
    
    public function diff($a, $b) {        
        $total = $a - $b;
        echo $a.' - '.$b.' = '.$total;
        echo '<br>';
        //return $total;
    }
    
    public function format($a, $b=2) {        
        
        $total = number_format($a, $b, '.', ',');
        return $total;
    }
    
    public function money($a) {
        
        setlocale(LC_MONETARY, 'en_US.UTF-8');       
        $total = money_format('%(#10n', $a);
        return $total;
    }

}

$operaciones = new Numbers();
$operaciones->sum(2, 3);
$operaciones->diff(2, 4);
echo $operaciones->format(1262.25361154, 4);
echo '<br>';
echo $operaciones->format(1262.25361154);
echo '<br>';
echo $operaciones->money(1262.25361154);