<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class NumbersConfig{
    
    private static $decimals = 4;
    private static $decimal_separator = '.';
    private static $thousands_separator = ' ';

    static function decimal(){
        
        return self::$decimals;
    }
    
    static function dec_sep(){
        
        return self::$decimal_separator;
    }
    
    static function mil_sep(){
        
        return self::$thousands_separator;
    }
}

class Numbers {
    
    
    static function sum($a, $b) {        
        $total = $a + $b;
        echo $a.' + '.$b.' = '.$total;
        echo '<br>';
        //return $total;
    }
    
    static function diff($a, $b) {        
        $total = $a - $b;
        echo $a.' - '.$b.' = '.$total;
        echo '<br>';
        //return $total;
    }
    
    static function format($a, $b=2) {  
        
        $dec = NumbersConfig::dec_sep();
        $mil = NumbersConfig::mil_sep();
        $nde = NumbersConfig::decimal();
        
        $total = number_format($a, $nde, $dec, $mil);
        return $total;
    }
    
    static function money($a) {
        
        
        
        setlocale(LC_MONETARY, 'en_US.UTF-8');       
        $total = money_format('%.2n', $a);
        return $total;
    }

}

echo Numbers::format( 1262.25361154);
echo '<br>';
echo Numbers::money( 1262.25361154);
