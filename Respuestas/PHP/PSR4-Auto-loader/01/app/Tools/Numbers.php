<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App;

class NumbersConfig{
    
    private static $decimals = 4;
    private static $decimal_separator = '.';
    private static $thousands_separator = ' ';

    static function decimal(){
        
        return self::$decimals;
    }
    
    static function dec_sep(){
        
        return self::$decimal_separator;
    }
    
    static function mil_sep(){
        
        return self::$thousands_separator;
    }
}

class Operations extends Numbers{
    
    private static $total;
    
    public function multi($a, $b) {        
        self::$total = $a * $b;
        echo $a.' * '.$b.' = '.self::$total;
        echo '<br>';
        //return $total;
    }
    
    public function div($a, $b) {        
        self::$total = $a / $b;
        echo $a.' / '.$b.' = '.self::$total;
        echo '<br>';
        //return $total;
    }
}

class Numbers {

    private static $total;
    
    public function sum($a, $b) {        
        self::$total = $a + $b;
        echo $a.' + '.$b.' = '.self::$total;
        echo '<br>';
        //return $total;
    }
    
    public function diff($a, $b) {        
        self::$total = $a - $b;
        echo $a.' - '.$b.' = '.self::$total;
        echo '<br>';
        //return $total;
    }
    
    public function format($a, $b=2) {  
        
        $dec = NumbersConfig::dec_sep();
        $mil = NumbersConfig::mil_sep();
        $nde = NumbersConfig::decimal();
        
        self::$total = number_format($a, $nde, $dec, $mil);
     
        return self::$total;
    }
    
    public function money($a) {
        
        setlocale(LC_MONETARY, 'en_US.UTF-8');       
        self::$total = money_format('%(#10n', $a);
        return self::$total;
    }

}

echo 'llamada a Numbers: ';
echo Numbers::format( 1262.25361154);