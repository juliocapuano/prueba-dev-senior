<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$matrix = [
    ['user' => 'A', 'name' => 'a a', 'age' => 17],
    ['user' => 'B', 'name' => 'b b', 'age' => 22],
    ['user' => 'C', 'name' => 'c c', 'age' => 36],
    ['user' => 'D', 'name' => 'd d', 'age' => 42],
    ['user' => 'E', 'name' => 'e e', 'age' => 12],
];

//$matrix = array(
//    array('user' => 'A', 'name' => 'a a', 'age' => 17),
//    array('user' => 'B', 'name' => 'b b', 'age' => 22),
//    array('user' => 'C', 'name' => 'c c', 'age' => 36),
//    array('user' => 'D', 'name' => 'd d', 'age' => 42),
//    array('user' => 'E', 'name' => 'e e', 'age' => 12),
//);

function suma($v, $w) {
    $v += $w;
    return $v;
}

function mayor($var) {
//    if($var > 30){
//        
//    }

    return $var > 30;
}

function recorrer($c) {
    foreach ($c as $key => $value) {
        $val = $val . $c[$key] . ' ';
    }
    return $val;
}

function array_edades($matrix) {
    foreach ($matrix as $indice => $fila) {

        foreach ($fila as $key => $value) {
            
        }
        //echo $fila['age'];
        //echo "<br>";
        $elem = $fila['age'] . ',' . $elem;
    }

    $elem = trim($elem, ',');
    $array = explode(",", $elem);

    return $array;
}

function array_tabla($matrix) {
    echo '<html>';
    echo '<head>';
    echo'<title>Prueba de Arrays</title>';
    echo '</head>';
    echo '<body>';
    echo '<table border="1">';
    echo '<tr>';
    echo '<td>';
    echo 'user';
    echo '</td>';
    echo '<td>';
    echo 'name';
    echo '</td>';
    echo '<td>';
    echo 'age';
    echo '</td>';
    echo '</tr>';

    foreach ($matrix as $indice => $fila) {
        echo '<tr>';
        echo '<td>';
        echo $fila['user'];
        echo '</td>';
        echo '<td>';
        echo $fila['name'];
        echo '</td>';
        echo '<td>';
        echo $fila['age'];
        echo '</td>';
        echo "<br>";
    }

    echo '</tr>';
    echo '</table>';
    echo '';
    echo '</body>';
    echo '</html>';
}

function may($a, $b){
    return $a > 30;
}

$array = array_edades($matrix);
$sumato = array_reduce($array, "suma");
$media = $sumato / count($array);
$mayores = array_filter($array, "mayor");
$marca_may = array_map("may", $matrix);
$nuevo_campo = array_push($matrix, $marca_may);

echo 'Usando la función array_reduce calcular promedio de edades de $matrix: ' . $media;
echo '<br>';
echo 'Usando la función array_filter solo los mayores de 30 años $matrix: ' . recorrer($mayores);
echo '<br>';
echo 'iterar dentro de un loop foreach para mostrar la data en una tabla: ';
echo '<br>';
echo array_tabla($matrix);
echo '<br>';
echo 'Usando la función array_map transformar cada indice dentro de $matrix para agregar la marca de mayor de edad';
echo '<br>';
echo "['user'=>'zzzz', 'name'=>'z z', 'age'=>99, 'adult'=> true]";
echo '<br>';
echo 'y guardar en una nueva variable $matrix_calc: ';
echo '<br>';
//print_r($nuevo_campo);
//var_dump($marca_may);

//$input = array(
//    array(
//        'first' => 1,
//        'second' => 2,
//    ),
//    array(
//        'third' => 3
//    )
//);

echo 'resultado '.max(array_map( function( $arr ) {
 
    return max($arr);
}, $matrix)); 

$mayor_edad = max(array_map( function( $arr ) {
 
    return max($arr);
}, $matrix)); 

//$matrix['age']=>42['adult'] = true;

var_dump($mayor_edad);