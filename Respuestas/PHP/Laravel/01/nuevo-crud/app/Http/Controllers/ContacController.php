<?php

namespace App\Http\Controllers;

use App\Models\Contac;
use Illuminate\Http\Request;

class ContacController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['contactos']=Contac::paginate(5);
        return view('contac.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('contac.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         //$datoscontac = request()->all();
         $datoscontac = request()->except('_token');
         Contac::insert($datoscontac);
         return redirect('/contac');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contac  $contac
     * @return \Illuminate\Http\Response
     */
    public function show(Contac $contac)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contac  $contac
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $datos=Contac::findOrFail($id);
        return view('contac.edit', compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contac  $contac
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datoscontac = request()->except('_token','_method');
        Contac::where('id','=',$id)->update($datoscontac);

        return redirect('/contac');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contac  $contac
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Contac::destroy($id);
        return redirect('/contac');
    }
}
