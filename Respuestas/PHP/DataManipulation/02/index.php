<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// $cadena = '/Respuestas/PHP/DataManipulation/02/';

// function total_carac($cadena) {
//     $arr1 = str_split($cadena);
//     $duple = array_count_values($arr1);

//     foreach ($duple as $key => $value) {

//         if (ctype_alpha($key)) {
//             //echo $key.' = '.$value;
//             $letras = $letras + $value;
//         }
//     }
//     return $letras;
// }

// function total_carac_may($cadena) {
//     $arr1 = str_split($cadena);
//     $duple = array_count_values($arr1);

//     foreach ($duple as $key => $value) {

//         if ((ctype_alpha($key)) and (ctype_upper($key))) {
//             //echo $key.' = '.$value;
//             $letras = $letras + $value;
//         }
//     }
//     return $letras;
// }


// function snake_case($cadena){

// //    $texto = preg_replace('/', '_', strtolower($cadena));
//     $texto = str_replace('/', '_', $cadena);
//     return $texto;
// }

// echo 'Conteo de caracteres: ' . strlen($cadena);
// echo '<br>';
// echo 'total de letras: ' . total_carac($cadena);
// echo '<br>';
// echo 'Conteo de Letras solo en Mayúscula: '.total_carac_may($cadena);
// echo '<br>';
// echo 'Reemplazar las palabras a formato snake_case: '.snake_case($cadena);



// ## 02 - Cadenas

// Dada la cadena `/Respuestas/PHP/DataManipulation/02/`

$cadena = '/Respuestas/PHP/DataManipulation/02/';
// Obtener:

// - Conteo de caracteres
echo strlen($cadena) . PHP_EOL;
// - Conteo de Letras
echo strlen(preg_replace('/[^A-Z]/i', '', $cadena)) . PHP_EOL; // aqui se limpian todas las cosas que no sean letras
// - Conteo de Letras solo en Mayúscula
echo strlen(preg_replace('/[^A-Z]/', '', $cadena)) . PHP_EOL; // se quita la insensibilidaad de mayusculas y minusculas de carcteres /{meta}/i
// - Reemplazar las palabras a formato `snake_case`
$cadena_con_guiones =  preg_replace('/([A-Z])/', '_$1', $cadena); //se agrega un guion a las mayusculas
$cadena_en_minuscula =  strtolower($cadena_con_guiones);   //se cambia a miniscula
$cadena_final = str_replace('/_', '/', $cadena_en_minuscula);  // se corrige el gion del inicio de cada segmento

echo $cadena_final . PHP_EOL;
