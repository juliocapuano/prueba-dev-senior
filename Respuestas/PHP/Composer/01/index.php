<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require 'vendor/autoload.php';

use Carbon\Carbon;

printf("Dia de Hoy: %s", Carbon::now());

echo '<br>';
$AddWeek  = Carbon::now()->addWeek();
$subMonth = Carbon::now()->subMonth(3);
echo 'Adicionar una Semana: '.$AddWeek;
echo '<br>';
echo 'Restar 3 meses a la fecha: '.$subMonth;
echo '<br>';
echo 'Presentar la fecha en formato Mes/Dia/Año: ';
$dt = Carbon::now();
echo $dt->format('m/d/Y');
echo '<br>';
echo 'Calcular el numero de dias entre tu dia de nacimiento y la fecha actual: ';

$date = Carbon::createFromDate(1970,6,18)->diffInDays();
echo $date;
echo '<br>';
$calculo = "23-08-2021" - "22-07-2021";
echo $calculo;